#include <stdio.h>

char button;
bool finish = false;

int posI, posJ;
	
int map[10][11] = {
	{0,1,1,1,1,1,1,1,1,0,-1},
	{0,3,2,2,2,1,2,2,2,0,-1},
	{0,1,1,1,2,1,2,1,2,0,-1},
	{0,1,1,1,2,2,2,1,2,0,-1},
	{0,2,1,1,1,1,1,1,2,0,-1},
	{0,2,2,2,1,2,1,2,2,0,-1},
	{0,2,1,2,2,2,1,2,1,0,-1},
	{0,2,1,1,2,1,1,2,1,0,-1},
	{0,4,1,2,2,2,2,2,1,0,-1},
	{0,1,1,1,1,1,1,1,1,0,-1},
};

void drawMap();
void checkButton();
void clr();

main() {
	clr();
	drawMap();
	while(!finish) {
		checkButton();	
	}
	if(finish) printf("\n\nYOU WIN!");
	
	
}

void drawMap() {
		printf("  123456789\n");
	
		for(int i = 0; i < 10; i++) {
			printf("%d", i);
			for(int j = 0; j < 11; j++) {
				if(map[i][j] == 0) printf("%c", char(221));
					else if(map[i][j] == 1)  printf("%c", char(176));
					else if(map[i][j] == -1)  printf("\n");
					else if(map[i][j] == 3) {
						printf("+");
						posI = i;
						posJ = j;
					} else if(map[i][j] == 4)  printf("%c", char(45));
					else  printf(" "); 
			}
		}
}

void checkButton() {
	printf("You command > ");
	scanf("%c", &button);	
		
	switch(button) {
		case 'w':
			//printf("up");
			if(map[posI-1][posJ] == 4) {
				//printf("finish");
				finish = true;
				break;
			}
			
			if(map[posI-1][posJ] == 2) {
				//printf("yes");
				map[posI][posJ] = 2;
				map[posI-1][posJ] = 3;
				clr();
				drawMap();	
				break;
			} else {
				printf("ERROR: It is impossible to move \n\n");
			}		
			break;
		case 's':
			//printf("down");
			if(map[posI+1][posJ] == 4) {
				//printf("finish");
				finish = true;
				break;
			}
			
			if(map[posI+1][posJ] == 2) {
				//printf("yes");
				map[posI][posJ] = 2;
				map[posI+1][posJ] = 3;
				clr();
				drawMap();	
			} else {
				printf("ERROR: It is impossible to move \n\n");
			}	
			break;
		case 'd':
			//right
			if(map[posI][posJ+1] == 4) {
				//printf("finish");
				finish = true;
				break;
			}
			
			if(map[posI][posJ+1] == 2) {
				//printf("yes \n");
				map[posI][posJ] = 2;
				map[posI][posJ+1] = 3;
				clr();
				drawMap();
				break;
			} else {
				printf("ERROR: It is impossible to move \n\n");
				break;
			}
			break;
		case 'a':
			//left
			if(map[posI][posJ-1] == 4) {
				//printf("finish");
				finish = true;
				break;
			}
			
			if(map[posI][posJ-1] == 2) {
				//printf("yes");
				map[posI][posJ] = 2;
				map[posI][posJ-1] = 3;
				clr();
				drawMap();				
			} else {
				printf("ERROR: It is impossible to move \n\n");
			}
			break;
			/*
		default:
			printf("Unknown command");
			break;
			*/
	}		
}

void clr() {
	printf("\n \n \n \n \n \n \n \n \n \n \n \n \n");
}